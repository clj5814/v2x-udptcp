package cn.i96688.test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Scanner;

import com.google.gson.Gson;

import cn.i96688.v2x.utils.MsgUtil;
import cn.i96688.v2x.v2xframe.MessageFrame;
import cn.i96688.v2x.v2xframe.Response;
import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.util.AttributeKey;

public class TestTcpClient {
	public static final String TOKEN = "8E1A5A50";
	public static String AESKEY="28D5ACA2EBED4E49";
	public static int PORT=8401;
	public static void main(String[] args) {
		test1();
	}
	static void test1() {
		ClientPool pool=new ClientPool();
		try {
			System.out.println("0：退出\n2：登录\n3：心跳\n4：发送BSM\n输入选项后按回车键\n");
			@SuppressWarnings("resource")
			Scanner scan = new Scanner(System.in);
			while(scan.hasNextInt()) {
				int i=scan.nextInt();
				if(i==0) {
					break;
				}else if(i==2){//登录
					pool.sendMsg(TestData.getLogin(),MessageFrame.MSG_LOGIN);
				}else if(i==3) {//心跳
					pool.sendMsg(TestData.getHeartBeat(),MessageFrame.MSG_HEARTBEAT);
				}else if(i==4){//发送BSM
					pool.sendMsg(TestData.getBSM(),MessageFrame.MSG_SERVICE);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		pool.shutdown();
	}
	
	static class ClientPool{
		EventLoopGroup group = new NioEventLoopGroup();
		Bootstrap b = new Bootstrap();
		Channel ch =null;
		TcpClientChannel handler=new TcpClientChannel();
		ClientPool(){
			try {
	            b.group(group)
	             .channel(NioSocketChannel.class)
	             .handler(new ChannelInitializer<NioSocketChannel>() {
					@Override
					protected void initChannel(NioSocketChannel ch) throws Exception {
						ch.pipeline().addLast(
								new DelimiterBasedFrameDecoder(1024, Unpooled.copiedBuffer(new byte[] { 0x7e }),
										Unpooled.copiedBuffer(new byte[] { 0x7e, 0x7e })));
						ch.pipeline().addLast(handler);
					}});
				ch =b.connect("127.0.0.1", PORT).sync().channel();
				
//				ch.closeFuture().sync();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		void sendMsg(byte[] msg,int msgType) throws Exception {
			AttributeKey<Result> result = AttributeKey.valueOf("Result");
			ch.attr(result).set(new Result());
			AttributeKey<Integer> typeKey=AttributeKey.valueOf("typeKey");
			ch.attr(typeKey).set(msgType);
			ByteBuf buf=Unpooled.buffer(msg.length+1);
			buf.writeBytes(msg);//消息体
			buf.writeByte(MsgUtil.DELIMITER);//消息分割符
			ch.writeAndFlush(buf).sync();
		}
		void shutdown(){
			group.shutdownGracefully();
		}
	}
	static class TcpClientChannel extends SimpleChannelInboundHandler<ByteBuf>{
		
		@Override
		protected void channelRead0(ChannelHandlerContext ctx, ByteBuf msg) throws Exception {
			AttributeKey<Result> result = AttributeKey.valueOf("Result");
			Result call=ctx.channel().attr(result).get();
			AttributeKey<Integer> typeKey=AttributeKey.valueOf("typeKey");
			Integer msgType=ctx.channel().attr(typeKey).get();
			if(call!=null) {
				byte[] bs=new byte[msg.readableBytes()];
				msg.readBytes(bs);
				call.test(bs,msgType);
			}
		}
		@Override
	    public void exceptionCaught(ChannelHandlerContext channelHandlerContext, Throwable cause){
	        cause.printStackTrace();
	        channelHandlerContext.close();
	    }
	}
	static class Result{
		public void test(byte[] bs,int msgType) {
			try {
				Response resp = Response.per_decode(false,new ByteArrayInputStream(bs));
				String desc="";
				switch(msgType) {
				case MessageFrame.MSG_LOGIN:
					desc="登录测试";
					break;
				case MessageFrame.MSG_HEARTBEAT:
					desc="心跳测试";
					break;
				case MessageFrame.MSG_SERVICE:
					desc="业务测试";
					break;
				default:
					break;
				}
				System.out.println("终端收到（"+desc+"）返回消息："+new  Gson().toJson(resp));
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
	}
}
