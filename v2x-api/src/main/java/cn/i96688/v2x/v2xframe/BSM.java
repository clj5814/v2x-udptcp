/*
 * Generated by ASN.1 Java Compiler (https://www.asnlab.org/)
 * From ASN.1 module "V2xFrame"
 */
package cn.i96688.v2x.v2xframe;

import java.io.*;
import java.math.*;
import javax.validation.constraints.*;
import org.asnlab.asndt.runtime.conv.*;
import org.asnlab.asndt.runtime.conv.annotation.*;
import org.asnlab.asndt.runtime.type.AsnType;
import org.asnlab.asndt.runtime.value.*;

public class BSM {

	@NotNull
	@Component(0)
	public Integer msgCnt;

	@NotNull
	@Size(min=8, max=8)
	@Component(1)
	public byte[] id;

	@Null
	@Size(min=4, max=16)
	@Component(2)
	public byte[] plateNo;	/* OPTIONAL */

	@NotNull
	@Component(3)
	public Integer secMark;

	@NotNull
	@Component(4)
	public Position3D pos;

	@NotNull
	@Component(5)
	public PositionConfidenceSet accuracy;

	@NotNull
	@Component(6)
	public TransmissionState transmission;

	@NotNull
	@Component(7)
	public Integer speed;

	@NotNull
	@Component(8)
	public Integer heading;

	@Null
	@Component(9)
	public Integer angle;	/* OPTIONAL */

	@Null
	@Component(10)
	public MotionConfidenceSet motionCfid;	/* OPTIONAL */

	@NotNull
	@Component(11)
	public AccelerationSet4Way accelSet;

	@NotNull
	@Component(12)
	public BrakeSystemStatus brakes;

	@NotNull
	@Component(13)
	public VehicleSize size;

	@NotNull
	@Component(14)
	public VehicleClassification vehicleClass;

	@Null
	@Component(15)
	public VehicleSafetyExtensions safetyExt;	/* OPTIONAL */


	public boolean equals(Object obj) {
		if(!(obj instanceof BSM)){
			return false;
		}
		return TYPE.equals(this, obj, CONV);
	}

	public void per_encode(boolean align, OutputStream out) throws IOException {
		TYPE.encode(this, align? EncodingRules.ALIGNED_PACKED_ENCODING_RULES:EncodingRules.UNALIGNED_PACKED_ENCODING_RULES, CONV, out);
	}

	public static BSM per_decode(boolean align, InputStream in) throws IOException {
		return (BSM)TYPE.decode(in, align? EncodingRules.ALIGNED_PACKED_ENCODING_RULES:EncodingRules.UNALIGNED_PACKED_ENCODING_RULES, CONV);
	}


	public final static AsnType TYPE = V2xFrame.type(65616);

	public final static CompositeConverter CONV;

	static {
		CONV = new AnnotationCompositeConverter(BSM.class);
		AsnConverter msgCntConverter = MsgCount.CONV;
		AsnConverter idConverter = OctetStringConverter.INSTANCE;
		AsnConverter plateNoConverter = OctetStringConverter.INSTANCE;
		AsnConverter secMarkConverter = DSecond.CONV;
		AsnConverter posConverter = Position3D.CONV;
		AsnConverter accuracyConverter = PositionConfidenceSet.CONV;
		AsnConverter transmissionConverter = TransmissionState.CONV;
		AsnConverter speedConverter = Speed.CONV;
		AsnConverter headingConverter = Heading.CONV;
		AsnConverter angleConverter = SteeringWheelAngle.CONV;
		AsnConverter motionCfidConverter = MotionConfidenceSet.CONV;
		AsnConverter accelSetConverter = AccelerationSet4Way.CONV;
		AsnConverter brakesConverter = BrakeSystemStatus.CONV;
		AsnConverter sizeConverter = VehicleSize.CONV;
		AsnConverter vehicleClassConverter = VehicleClassification.CONV;
		AsnConverter safetyExtConverter = VehicleSafetyExtensions.CONV;
		CONV.setComponentConverters(new AsnConverter[] { msgCntConverter, idConverter, plateNoConverter, secMarkConverter, posConverter, accuracyConverter, transmissionConverter, speedConverter, headingConverter, angleConverter, motionCfidConverter, accelSetConverter, brakesConverter, sizeConverter, vehicleClassConverter, safetyExtConverter });
	}


}
