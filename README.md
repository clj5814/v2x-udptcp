# 车联网TCP-UDP数据接收网关

#### 介绍
车联网V2X数据接入实时性太高，一个中等城市并发量能到每秒百万至千万数据之间，数据传输协议使用TCP、UDP，所以数据接入网关使用netty非常适合。本程序使用netty实现TCP、UDP服务，数据报文协议采用ASN.1，运行框架采用spring cloud+dubbo，netty接收数据使用rpc传递给业务子系统处理

#### 软件架构
* Spring boot 2.2.2.RELEASE
* netty 4.1.43
* dubbo 2.7.5
* 服务治理：nacos-server-1.2.0
* 数据报文协议：ASN.1

#### 目录说明
* v2x-api：服务接口、ASN.1报文实现类
* v2x-service：一个简单业务模拟实现
* v2x-gateway：TCP、UDP数据接收网关
* v2x-registry：服务注册、服务发现
* v2x-test-termination：一个简单模拟终端发送数据
* ASN.1：ASN实现类需要用到的jar包

#### 使用说明
1.  进入ASN.1，运行命令导入：mvn install:install-file -Dfile=asnrt.jar -DgroupId=org.asnlab -DartifactId=org-asnlab -Dversion=1.0.0 -Dpackaging=jar 
1.  运行命令启动注册中心服务：v2x-registry\nacos\bin\startup
1.  启动v2x-service、v2x-gateway
1.  进入v2x-test-termination运行相关测试类

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
