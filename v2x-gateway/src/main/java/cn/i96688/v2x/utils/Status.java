package cn.i96688.v2x.utils;
/**
 * 操作状态
 * @author xuyuansheng
 *
 */
public class Status {
	public final static int SUCCESS=0; 
	public final static int FAIL=1;
	
	private String msg;
	private int code;
	/**
	 * 处理成功json提示
	 * @param msg
	 * @return
	 */
	public static Status success(String msg) {
		Status s=new Status();
		s.code=Status.SUCCESS;
		s.msg=msg;
		return s;
	}
	/**
	 * 处理失败json提示
	 * @param msg
	 * @return
	 */
	public static Status fail(String msg) {
		Status s=new Status();
		s.code=Status.FAIL;
		s.msg=msg;
		return s;
	}
	public static Status newStatus(boolean b,String msg) {
		return b?success(msg):fail(msg);
	}
	public static boolean isFail(int code) {
		return Status.FAIL==code;
	}
	public boolean isFail() {
		return this.code==Status.FAIL;
	}
	public String toJsonString() {
		StringBuilder sb=new StringBuilder();
		sb.append("{\"code\":").append(this.code).append(",\"msg\":\"").append(this.msg).append("\"}");
		return sb.toString();
	}
}
