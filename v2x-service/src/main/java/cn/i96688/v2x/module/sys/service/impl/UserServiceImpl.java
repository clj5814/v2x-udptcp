package cn.i96688.v2x.module.sys.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.dubbo.config.annotation.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.i96688.v2x.api.AuthService;
import cn.i96688.v2x.module.sys.service.UserService;
import cn.i96688.v2x.v2xframe.Response;
@Service
public class UserServiceImpl implements AuthService, UserService {
	private final static Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);
	private Map<String,String> users;
	
	@PostConstruct
	public void init() {
		users=new HashMap<>();
		users.put("1101101102450001", "abc123");
		users.put("1101101102450002", "abc123");
		users.put("1101101102450003", "abc123");
	}
	@Override
	public Response login(String id, String password) {
		LOGGER.info("设备登录开始");
		Response resp=null;
		try {
			String pass=users.get(id);
			if(pass==null) {
				resp=Response.fail("登录用户不存在");
			}else if(pass.equals(password)) {
				resp=Response.success("登录成功");
			}else {
				resp=Response.fail("密码错误");
			}
			LOGGER.info("设备登录完成");
			
		}catch(Exception e) {
			resp=Response.fail("登录出错");
			LOGGER.error("登录出错",e);
		}
		return resp;
		
	}
	
}
