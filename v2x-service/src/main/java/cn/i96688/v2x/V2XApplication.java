package cn.i96688.v2x;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class V2XApplication {

    public static void main(String[] args) {
    	SpringApplication.run(V2XApplication.class, args);
    }

}
